<?php require_once( '../couch/cms.php' ); ?>
<cms:embed 'header.php' />
<cms:embed 'navbar.php' />
<html lang="en">
  <head>
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="186963805865-vtp8e8fsim8cc0jpbdjf9s6kso6j0bpt.apps.googleusercontent.com">
<script src="https://apis.google.com/js/platform.js" async defer></script>

  </head>
  
    <cms:template title='Login' hidden='1' />

<center><br><br>
<div class="box" style="width:50%">
    
<div id="logout_msg" class="message is-danger" style="">
    <div class="message-header">
    Hello this is for you.
    </div>
    <div class="message-body">
    You need to login to get most out of this portal. <br>
    Keep learning. <br>
    Peace. _/\_
    </div>
</div>
    

<div id="login_msg" class="message is-success" style="display:none;">
    <div class="message-header">
    Hello Manu
    </div>
    <div class="message-body">
    Welcome to the DSSSB.TK platform <br>
    Keep learning. <br>
    Peace. _/\_
    </div>
</div>

    <hr>
    
<div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
</div>
</center>

    <!-- now the real work -->
    <cms:if k_logged_in >

        <!-- this 'login' template also handles 'logout' requests. Check if this is so -->
        <cms:set action="<cms:gpc 'act' method='get'/>" />
    
        <cms:if action='logout' >
            <cms:process_logout />
        <cms:else />  
            <!-- what is an already logged-in member doing on the login page? Send back to homepage. -->
            <cms:redirect k_site_link />
        </cms:if>
    
    <cms:else />

<div id="form">
        <!-- show the login form -->
        <h1>Login</h1>
            <cms:form method='post' anchor='0' >
            <cms:if k_success >
                <!-- 
                    The 'process_login' tag below expects fields named 
                    'k_user_name', 'k_user_pwd' and (optionally) 'k_user_remember', 'k_cookie_test'
                    in the form
                -->
                                <cms:process_login />
            </cms:if>
            
            <cms:if k_error >
                <h3><font color='red'><cms:show k_error /></font></h3>
            </cms:if>
            
            
            Username: <br/>
            <cms:input type='text' name='k_user_name' /> <br/>
            
            Password: <br />
            <cms:input type='password' name='k_user_pwd' /> <br/>
            
            <!-- if 'remember me' function is not required, the following checkbox can be omitted -->
            <cms:input type='checkbox' name='k_user_remember' opt_values='Remember me=1' /> <br/>
            
            <input type="hidden" name="k_cookie_test" value="1" />
            
            <input type="submit" value="Login" name="submit"/>
        </cms:form>
            <!-- optionally display links for 'Create account' and 'Lost password' -->

       <cms:ignore>
        <cms:if k_user_registration_template >
            Not signed up yet? <a href="<cms:link k_user_registration_template />" />Create an account here.</a> <br>
        </cms:if>
        <cms:if k_user_lost_password_template >
            Forgot your password? <a href="<cms:link k_user_lost_password_template />" />Click here.</a> <br>
        </cms:if>
        </cms:ignore>
</div> 


<script>
function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());

    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);
    
    // Create Base64 Object
    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/++[++^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
    
    // Define the string
    var email = profile.getEmail();
    var id = profile.getId();
    var name = profile.getName();
    var photo = profile.getImageUrl();
    
    // Encode the String
    var email_en = Base64.encode(email);
    var id_en = Base64.encode(id);
    var name_en = Base64.encode(name);
    var photo_en = Base64.encode(photo);

    document.getElementById("logout_msg").style = "display:none;";
    document.getElementById("login_msg").style = "";
    
    document.getElementById("login_msg").innerHTML = `<div class="message-header"> Hello `+ name +`</div>    <div class="message-body">    Welcome to the DSSSB.TK platform <br>    Keep learning. <br>Peace. _/\_</div>` ;
    
    var loginToast = mdtoast('Login Suceesful. Redirecting...', { duration: 2000, init: true }); 
    loginToast.show();

    var url = "_OAuth2.php?emenco=" + email_en + "&idenco=" + id_en + "&photo=" + photo_en + "&nameenco=" + name_en;
    console.log(url);
    window.setTimeout(function(){window.location.href = url;}, 2000);

    
}
</script>


<script>
    
var x = document.getElementById("form");
{
x.style.display = "none";
}
</script>
    </cms:if>
<?php COUCH::invoke(); ?>