<?php require_once( '../couch/cms.php' ); ?>
    <cms:template title='Registration' hidden='1' />

    <cms:set emdeco_r="<cms:php>
        $emenco = '<cms:gpc "emenco" />';
        echo base64_decode($emenco);
    </cms:php>" />
    
    <cms:set iddeco_r="<cms:php>
        $idenco = '<cms:gpc "idenco" />';
        echo base64_decode($idenco);
    </cms:php>" />
    
    <cms:set namedeco_r="<cms:php>
        $nameenco = '<cms:gpc "nameenco" />';
        echo base64_decode($nameenco);
    </cms:php>" />
    
    <cms:set photodeco_r="<cms:php>
        $photo = '<cms:gpc "photo" />';
        echo base64_decode($photo);
    </cms:php>" />
    
        <cms:if k_logged_in >
            <!-- what is an already logged-in member doing on this page? Send back to homepage. -->
            <cms:redirect k_site_link />
        </cms:if>
    
    <cms:set ip_address="<cms:php>
        function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    echo getUserIpAddr();
    </cms:php>" />
    
    <cms:db_persist 
        _masterpage="users/index.php"
        _mode='create'
        _separator='|'
        _invalidate_cache='0'
        _auto_title='0'
        
        _invalidate_cache='0'
        k_page_name = "<cms:random_name />"
        k_publish_date = "<cms:date format="%F %T" />"
    
        extended_user_email="<cms:show emdeco_r />"
        extended_user_password="<cms:show iddeco_r />"
        extended_user_password_repeat="<cms:show iddeco_r />"
        
        k_page_title="<cms:show namedeco_r />"
        profile_img="<cms:show photodeco_r />"
        google_o_auth_id="<cms:show iddeco_r />"
        user_ip="<cms:show ip_address />"
    >  
    
        <cms:if k_error >
            <font color='red'>ERROR:
            <cms:each k_error >
                <cms:show item /><br>
            </cms:each>
            </font>
        <cms:else />
            <cms:show k_last_insert_id />
            <cms:show k_last_insert_page_name />
            
            <cms:redirect "<cms:show k_site_link />?attempt=regdone" />
        </cms:if>
        
    </cms:db_persist> 
    
<?php COUCH::invoke(); ?>