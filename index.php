<?php require_once( '../couch/cms.php' ); ?>
 <cms:embed 'header.php' />
 <cms:template clonable='1' title='Users' hidden='1'>
        <!-- If additional fields are required for users, they can be defined here in the usual manner. -->        
    <cms:editable name="google_o_auth_id" type="text" />
    <cms:editable name="profile_img" type="text" />
    <cms:editable name="user_ip" type="text" />
    <cms:editable name="user_last_login" type="text" />
    <cms:editable name="user_role" type="text" />
    <cms:editable name="user_role" opt_values='Student | Moderator | Teacher | Admin'  type='dropdown' />
</cms:template>
 
<cms:embed 'navbar.php' />    
    <div class="box">
<cms:if k_logged_in >
    <cms:if k_user_name="superadmin">

<div class="box">
<div class="table-container">
  <table class="table is-fullwidth">
<thead>
    <tr>
      <th>Name</th>
      <th>Email ID</th>
      <th>Photo</th>
      <th>I.P.</th>
      <th>Last time Login</th>
    </tr>
</thead>
<tbody>
   <cms:pages masterpage="users/index.php">
    <tr>
        <td><cms:show k_page_title /></td>
        <td><cms:show extended_user_email /></td>
        <td><img src="<cms:show profile_img />" /></td>
        <td><cms:show user_ip /></td>
        <cms:if user_last_login >
                <td id="time<cms:show k_page_id />">last_login -> <cms:show user_last_login /> and now -> <cms:date /></td>
                <script>
                    //var update = "2020-04-08 08:10:28";
                    var update = "<cms:show user_last_login />";
                    document.getElementById("time<cms:show k_page_id />").innerHTML = moment(update).fromNow();
                </script>
            <cms:else />
                <td id="time<cms:show k_page_id />">No Login Activity.</td>    
        </cms:if>
        
        
    </tr>
</cms:pages>  
</tbody>
  </table>
</div>

</div>
 <cms:else />
    
    <center>You are not authorised to access this page. sorry. You will be redirected to homepage in 3 seconds. Bye.</center>
    <meta http-equiv="refresh" content="3;url=<cms:show k_site_link />" />
    
    </cms:if>
</cms:if>    

</div>

<cms:embed 'footer.php' />    
<?php COUCH::invoke(); ?>